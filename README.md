# Cobrando S.A.S Prueba - Backend

Recuerde ejecutar

```
npm install en caso de ser necesario
```

Pasos para verificar todos los puntos requeridos 


1- Para verificar el login estos estos campos se hicieron el Postman y en su defecto este debe ser utilizado, para correr todas las pruebas 
```
Iniciar proyecto(backEnd)

npm run start:dev
Este se configuro para que corra en el puerto 3001 esto esta configurado en ".env"

```

2- Para crear un usuario si lo decean para poder validar el login(este genera un token)

```
Nos dirigimos a PostMan y con la siguiente ruta

http://localhost:3001/api/usuarios

En el body en tipo Raw y en formato JSON

agregamos un usario asi: 
{    
    "nombre": "prueba2",
    "password": "123",
    "email": "preuba2@cobrando.com.co"   
}

```

Pero en su defecto aqui ya tienen un usuario
```
En su defecto para la prueba seria:

    "nombre": "cobrandouser",
    "password": "123", <- Esta clave en la base de datos aparece cifrada
    "email": "cobrandouserg@cobrando.com.co",

```


3- LOGIN
```
Para poder ingresar a la ruta de subir archivo necesitan un token el cual es generado para usuarios que ya tengan una cuenta creada(anteriormente facilitada)

para ingregar y generar el token:

- Vamos a la ruta:
http://localhost:3001/api/login

- Vamos a Body, seleccionamos raw y formato JSON y diligenciamos de la siguiente manera

{

    "password": "123",
    "email": "cobrandouserg@cobrando.com.co"
}

Esto nos generara un token el cual no permitira navegar a la siguiente seccion de la prueba

en su defecto : este es el token en caso de no querer generarlo 

https://jwt.io/#debugger-io?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiI1ZjhiNDMxZTdiODA1NjI0OGNkNGM1ZTkiLCJpYXQiOjE2MDMwNDc3Mjd9.3tqbTEk2DTtIt5xZTIuv1tGtzoFU8aH6c9xNT15ogNI

```


4- Cargar el Excel

```
Mediante este link podremos ingresar a la ultima parte del API

Tipo: POST
http://localhost:3001/api/upload/usuarios/5f8b431e7b8056248cd4c5e9

Este no solicitara el id del usuario que es "5f8b431e7b8056248cd4c5e9" y en los headers un el token
Y para cargar el archivo en el body un form-data para subir el archivo de excel y este se carga a la fs y al mismo tiempo a la base datos MongoDB





```
se adjuntara un documento en el correo para mas detalles 



