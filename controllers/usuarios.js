const { response } = require('express');
const bcrypt = require('bcryptjs');


const Usuario = require('../models/usuario');
const { generarJWT } = require('../helpers/jwt');


const getUsuarios = async(req,res) =>{

    const desde = Number(req.query.desde) || 0;

    const [ usuarios, total ] = await Promise.all([
        Usuario
            .find({}, 'nombre email rol google img')
            .skip( desde )
            .limit( 5 ),

        Usuario.countDocuments()   

    ]);

    
    res.json({
        ok: true,
        usuarios,
        total        
    });

};


const crearUsuario = async(req,res = response) =>{

    const { email, password, nombre } = req.body;
   



    try {

        const existeEmail = await Usuario.findOne({ email });

         if ( existeEmail ) {
             return res.status(400).json({
                 ok: false,
                 msg: 'El correo ya esta registrado'
             });           
         }

          
         const usuario = new Usuario( req.body );

         // Encriptar Contraseña- el salt es data que sale de forma aleatorio

         const salt = bcrypt.genSaltSync();
         usuario.password = bcrypt.hashSync( password, salt );





            // Esto graba en la base de datos
            await usuario.save();

            // Generar un TOKEN - JWT
            const token = await generarJWT( usuario.id );
            

    
            res.json({
                ok: true,
                usuario,
                token,                
            });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Error inesperado... revisar logs'
        });
    }




};




module.exports = {
    getUsuarios,
    crearUsuario,
};