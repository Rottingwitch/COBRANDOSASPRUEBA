const path = require('path');
const fs = require('fs');
const xlsx = require('xlsx');



const { response, json } = require('express');
const { v4: uuidv4 } = require('uuid');
const  Jsonexcel  = require('../models/extojson');


const fileUpload = ( req, res = response ) => {

    const tipo = req.params.tipo;
    // Validar que exista un archivo
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).json({
            ok: false,
            msg: 'No hay ningun archivo'
        });
    }

    // Procesar el excel...
    const file = req.files.excel;


    // generar la extencion del archivo
    const nombreCortado = file.name.split('.'); // asdasd.1.2.jpg
    const extensionArchivo = nombreCortado[ nombreCortado.length - 1 ];

    // Validar Extensions
    const extensionesValida = ['csv', 'xml', 'xlsx',];
    if ( !extensionesValida.includes(extensionArchivo) ) {
        return res.status(400).json({
            ok: false,
            msg: 'No es una extension permitida'
        });                
    }

    // Genera el nombre del archivo
    const nombreArchivo = `${ uuidv4() }.${ extensionArchivo }`;

    // Path para guarda el excel en la carpeta Uploads
    const path = `./uploads/${ tipo }/${ nombreArchivo }`;

      // Mover el excel
    file.mv( path , async (err) => {
    if (err){
        console.log(err);
        return res.status(500).json({
            ok:false,
            msg:'Error al mover la imagen'
        });
    }
 

    // Actualizar base de datos

    const excel = xlsx.readFile( `./uploads/${ tipo }/${ nombreArchivo }` );

    const nombreHoja = excel.SheetNames;
    const datos = xlsx.utils.sheet_to_json(excel.Sheets[nombreHoja[0]]);
    // console.log(datos); 

    for (let i = 0; i < datos.length; i++) {
        // console.log(datos[i]);        
        const extojson = new Jsonexcel( datos[i] );
        await extojson.save();
    }

        res.json({
        ok: true,
        msg: 'Archivo subido',
        nombreArchivo,
        datos,        
    }); 
   



  });

    
};

module.exports = {   
     fileUpload, 
  
};