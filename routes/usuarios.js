
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');


const { getUsuarios, crearUsuario } = require('../controllers/usuarios');
const { validarJWT } = require('../middlewares/valida-jwt');

const router = Router();

// Consultar a la ruta GET
router.get( '/',validarJWT, getUsuarios );


// Crear usuario POST
router.post( '/',
    [
        
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('password', 'El password es obligatorio').not().isEmpty(),
        check('email', 'El email es obligatorio').isEmail(),
        validarCampos,

    ],
    crearUsuario
    
);




module.exports = router;
