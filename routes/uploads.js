
const { Router } = require('express');
const expressfileUpload = require('express-fileupload');


const { validarJWT } = require('../middlewares/valida-jwt');
const { fileUpload } = require('../controllers/uploads');


const router = Router();

router.use( expressfileUpload() );

// put del archivo al fs
router.post( '/:tipo/:id',validarJWT, fileUpload );



module.exports = router;