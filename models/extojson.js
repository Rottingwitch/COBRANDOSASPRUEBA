const { Schema, model } = require('mongoose');

const ExtojsonSchema = Schema({ 
    
    nombre:{
        type: String,
        // required: true
    },
    valorcobro:{
        type: Number,
        
    },
    fecha:{
        type: Date,
    }
    
});

module.exports = model( 'Jsonexcel', ExtojsonSchema );